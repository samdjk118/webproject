<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Login Page</title>
    <link rel="stylesheet" type= "text/css" href="style.css">
</head>
<body>
     <h1 class="Center">登入介面</h1>
     <div id="frm">
        <form action="process.php" method="POST">
            <p>
                <label>帳號:</label>
                <input type="text" id="user" name="user" />  
            </p>
            <p>
                <label>密碼:</label>
                <input type="password" id="password" name="pass" />  
            </p>
            <p>
                <input type="submit" id="btn" name="Login" />  
            </p>

        </form>
     </div>
     
</body>
</html>